
from sqlalchemy.orm import backref, relationship
from sqlalchemy import Integer, Column
from app.configs.database import db


tasks_categories = db.Table('tasks_categories',
    Column('id', Integer, primary_key=True),
    Column('task_id',Integer, db.ForeignKey('tasks_model.id')),
    Column( 'category_id', Integer, db.ForeignKey('categories.id')) 
)
    
    