from typing import Text
from flask.scaffold import F

from sqlalchemy.orm import backref, relationship
from app.configs.database import db
from sqlalchemy import Column, String, Text
from dataclasses import dataclass

@dataclass
class CategoriesModel(db.Model):

    name: str
    description: str

    __tablename__ ='categories'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), nullable=False, unique=True)   
    description = db.Column(db.String)

    