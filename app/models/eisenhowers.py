from re import S, T
from app.configs.database import db
from dataclasses import dataclass

@dataclass
class EisenhowersModel(db.Model):
 
    type: str

    __tablename__ = 'eisenhowers'

    id = db.Column(db.Integer, primary_key=True)
    type = db.Column(db.String(100))
