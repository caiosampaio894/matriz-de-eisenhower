from sqlalchemy.orm import backref, relationship
from app.configs.database import db
from dataclasses import dataclass, field
from app.models.categories_model import CategoriesModel
from app.models.tasks_categories import tasks_categories
@dataclass
class TasksModel(db.Model):

    id: int
    name: str
    description: str
    duration: int
    eisenhower_classification: str
    categories: list = field(default_factory=[])

    __tablename__ = 'tasks_model'


    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), nullable=False, unique=True )
    description = db.Column(db.String)
    duration = db.Column(db.Integer)
    importance = db.Column(db.Integer)
    urgency = db.Column(db.Integer)
    eisenhower_id = db.Column(db.Integer, db.ForeignKey('eisenhowers.id') , nullable=True)

    categories = relationship('CategoriesModel', secondary='tasks_categories', backref=backref('categories'))

    eisenhower_classification = relationship('EisenhowersModel')

        
        