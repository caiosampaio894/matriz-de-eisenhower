from re import S
from flask import Blueprint, request, current_app, jsonify
from sqlalchemy.exc import IntegrityError
from app.exc.errors import NotFound
from app.models.categories_model import CategoriesModel
from app.models.tasks_model import TasksModel
from app.models.tasks_categories import tasks_categories
from sqlalchemy.exc import IntegrityError
import pdb


bp_create_tasks = Blueprint('bp_create_tasks', __name__)

@bp_create_tasks.route('/task', methods=['POST'])
def create_tasks():    
    data = request.get_json()
    try:
        ex = {(1, 1): 1, 
            (1, 2): 2, 
            (2, 1): 3, 
            (2, 2): 4
            }
        data['eisenhower_id'] = ex[(data['importance'], data['urgency'])]
        categories = data.pop('categories')
        task = TasksModel(**data)
        # key_category = data['categories']
        session = current_app.db.session
        session.add(task)
        session.commit()

        
        for category in categories:
            print(category, '***************')
            myCategory = CategoriesModel.query.filter_by(name=category['name']).first()
            if myCategory:
                task.categories.append(myCategory)
            else:
                new_category = CategoriesModel(name=category['name'])
                session = current_app.db.session
                session.add(new_category)
                session.commit()
                my_task_categories = task.categories.append(new_category)
                session = current_app.db.session
                session.add(my_task_categories)
                session.commit()

    

        return jsonify(task), 201
    
    except IntegrityError:
        return {'message': 'Task already exists'}, 404

    except KeyError:
        return { 'error': {
            'valid_options':{
                'importance': [
                    1,
                    2
                ],
                'urgency': [
                    1,
                    2
                ]
            },
            'received_options': {
                'importance': data['importance'],
                'urgency': data['urgency']
            }
        }
    }
     

@bp_create_tasks.route('/task/<int:id>', methods=['PATCH'])
def update_task(id: int):
    data = request.json
   
    task = TasksModel.query.filter_by(id=id).update(data)

    if not task:
        return {'msg': 'task not found'}, 404
        
    current_app.db.session.commit()
    update_from_id = TasksModel.query.get(id)
    return jsonify(update_from_id), 200
        

@bp_create_tasks.route('/task/<int:id>', methods=['DELETE'])
def delete_task(id: int):
    try:
        task = TasksModel.query.get_or_404(id) 
        current_app.db.session.delete(task)
        current_app.db.session.commit()
        return '', 204
    except:
        return {'msg': 'task not found'}, 404
