from flask import Flask

def init_app(app:Flask) -> None:
    from .create_tasks import bp_create_tasks

    app.register_blueprint(bp_create_tasks)

    from .create_categories import bp_create_categories

    app.register_blueprint(bp_create_categories)

    