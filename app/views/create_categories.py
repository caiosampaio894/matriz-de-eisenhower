from flask import Blueprint, json, request, current_app, jsonify
from sqlalchemy.exc import IntegrityError
from sqlalchemy.sql.expression import except_
from app.models.categories_model import CategoriesModel
from app.exc.errors import NotFound
bp_create_categories = Blueprint('bp_create_categories', __name__)

@bp_create_categories.route('/category', methods=['POST'])
def create_category():
    data = request.get_json()

    try:
        category = CategoriesModel(**data)
        session = current_app.db.session
        session.add(category)
        session.commit()

        return jsonify(category), 201
    except IntegrityError:
        return {"message": "Key already exists!"}

@bp_create_categories.route('/category/<int:id>', methods=['PATCH'])
def update_category(id: int):
    data = request.json

    category = CategoriesModel.query.filter_by(id=id).update(data)

    if not category:
        return {'msg': 'category not found'}, 404

    current_app.db.session.commit()
    update_from_id =  CategoriesModel.query.get(id)

    return jsonify(update_from_id), 200



@bp_create_categories.route('/category/<int:id>', methods=['DELETE'])
def delete_category(id:int):

    category = CategoriesModel.query.get(id)

    if not category:
        return {'msg': 'category not found'}, 404

    current_app.db.session.delete(category)
    current_app.db.session.commit()

    return '', 204

        

